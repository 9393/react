import React, {Component} from 'react';

//Compoents
import Search from '../containers/search';

class App extends Component{
    render(){
        return(
            <div className="App">
                <Search/>
            </div>
        )
    }
}

export default App; 